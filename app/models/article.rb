class Article < ApplicationRecord
  has_many :comments, dependent: :destroy
  validates :title, presence: true, length: { minimum: 5 }
  # presence: true mengecek agar suatu attribut tidak kosong
end
